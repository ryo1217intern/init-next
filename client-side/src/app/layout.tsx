import "../app/globals.css"
import React, { ReactNode } from 'react';
import { ThemeProvider } from "@/app/themeProvider";
import { ClerkProvider } from "@clerk/nextjs"

interface RootLayoutProps {
  children: ReactNode;
}
import { Inter as FontSans } from "next/font/google"

import { cn } from "@/lib/utils"

const fontSans = FontSans({
  subsets: ["latin"],
  variable: "--font-sans",
})

export default function RootLayout({ children }: RootLayoutProps) {
  return (
    <ClerkProvider>
    <html lang="en" suppressHydrationWarning>
      <head />
      <body
        className={cn(
          "min-h-screen bg-background font-sans antialiased",
          fontSans.variable
        )}
      >
      <ThemeProvider
            attribute="class"
            defaultTheme="system"
            enableSystem
            disableTransitionOnChange
      >
        {children}
      </ThemeProvider>
      </body>
    </html>
    </ClerkProvider>
  )
}