"use client"
import { useUser } from "@clerk/nextjs"

export default function Home() {
  const { user } = useUser();
  return (
    <div className="h-screen w-screen flex items-center justify-center">
      <div>
        Welcome to your new Clerk app, {user ? user.fullName : "guest"}!
        <br />
        Your user ID is: {user ? user.id : "unknown"}.
      </div>
    </div>
  );
}
